﻿# Search Service App のアクティブ化を行う
try{
    Write-Host "Search Service App のアクティブ化を開始"
    $ssa = Get-SPEnterpriseSearchServiceApplication
    $active = Get-SPEnterpriseSearchTopology -SearchApplication $ssa -Active
    $clone = New-SPEnterpriseSearchTopology -SearchApplication $ssa -Clone -SearchTopology $active

    Set-SPEnterpriseSearchTopology -Identity $clone

    Get-SPEnterpriseSearchTopology -Active -SearchApplication $ssa

    Write-Host "アクティブ化完了"
}catch{
    Write-Host "異常終了"
    Write-Error($_.Exception)
}